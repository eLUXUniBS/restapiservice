from django.shortcuts import render
from . import models as db


# Create your views here.
def save_record(header=None, payload=None):
    if header is None and payload is None:
        return None

    obj = db.SensorDataModel.create(header=header, payload=payload)
    return obj


def query_record(header=None, payload=None, date=None):
    res = False
    try:
        if header is not None:
            res = db.SensorDataModel if res is False else res
            res = res.looking_header(params=header)
        if payload is not None:
            res = db.SensorDataModel if res is False else res
            res = res.looking_payload(params=payload)
        if date is not None:
            res = db.SensorDataModel if res is False else res
            res = res.looking_in_time(start=date.get("start", None), end=date.get("end", None))
    except Exception as e:
        print(e)
        return None
    return res


def get_last_record():
    return db.SensorDataModel.objects.last()

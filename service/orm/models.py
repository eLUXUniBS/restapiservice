from django.db import models
from uuid import uuid4
from datetime import datetime as dt


# Create your models here.

class SensorDataModel(models.Model):
    id = models.UUIDField(default=uuid4, primary_key=True, null=False, editable=False)
    ts_db = models.DateTimeField(default=dt.now(), null=False, editable=False)
    header = models.JSONField(default=dict, null=True)
    payload = models.JSONField(default=dict, null=True)

    class Meta:
        indexes = [
            models.Index(fields=["ts_db"])
        ]

    @classmethod
    def _do_query(cls, query, queryset=None):
        if queryset is None:
            queryset = cls.objects
        return queryset.filter(**query)

    @classmethod
    def create(cls, header=None, payload=None):
        if header is None or payload is None:
            return None
        obj = cls()
        obj.header = header
        obj.payload = payload
        obj.save()
        return obj

    @classmethod
    def looking_in_time(cls, start: str = None, end: str = None, **kwargs):
        query = dict()
        if start is not None and end is None:
            query = {"ts_db__gte": dt.strptime(start.strip() + " +0000", "%Y-%m-%d %H:%M %z")}
        elif end is not None and start is None:
            query = {"ts_db__lte": dt.strptime(end.strip() + " +0000", "%Y-%m-%d %H:%M %z")}
        elif start is not None and end is not None:
            query = {"ts_db__range": (
                dt.strptime(start.strip() + " +0000", "%Y-%m-%d %H:%M %z"),
                dt.strptime(end.strip() + " +0000", "%Y-%m-%d %H:%M %z"))}
        else:
            return None
        return cls._do_query(query=query, queryset=kwargs.get("queryset", None))

    @classmethod
    def looking_header(cls, params=None, **kwargs):
        query = dict()
        for k in params:
            query["header__" + k] = params[k]
        return cls._do_query(query=query, queryset=kwargs.get("queryset", None))

    @classmethod
    def looking_payload(cls, params=None, **kwargs):
        query = dict()
        for k in params:
            query["payload__" + k] = params[k]
        return cls._do_query(query=query, queryset=kwargs.get("queryset", None))

    def json(self):
        return dict(id=str(self.id), ts=str(self.ts_db), header=self.header, payload=self.payload)

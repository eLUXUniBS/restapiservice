
from django.views.decorators.csrf import csrf_exempt
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class eluxServiceTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user,*args,**kwargs):
        """
        Fornisce il token firmato, con tutte le proprietà del caso
        :param user:
        :param args:
        :param kwargs:
        :return:
        """
        token = super().get_token(user)
        # Add custom claims
        token['scheme_version'] = "1.0"
        token['roles']= "com"
        token["audience"] ="elux_app"
        return token


class eluxServiceTokenObtainPairView(TokenObtainPairView):
    serializer_class = eluxServiceTokenObtainPairSerializer

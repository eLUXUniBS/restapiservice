from django.db.models.query import QuerySet
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from orm import views as ctl_orm


def jsonize(obj):
    if obj is None:
        return None
    if type(obj) is QuerySet:
        obj=[x.json() for x in obj]
        return obj
    return obj.json()


class InterfaceOrmRetrieveLastData(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        buffer = ctl_orm.get_last_record()
        return Response(data=jsonize(buffer), status=status.HTTP_200_OK)


class InterfaceOrmSendData(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        buffer = ctl_orm.save_record(
            header=request.data.get("header", None),
            payload=request.data.get("payload", None)
        )
        return Response(data=jsonize(buffer), status=status.HTTP_200_OK)


class InterfaceOrmQueryData(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        print("REQUEST IS", request.data)
        buffer = ctl_orm.query_record(
            header=request.data.get("header", None),
            payload=request.data.get("payload", None),
            date=request.data.get("date",None)
        )
        return Response(data=jsonize(buffer), status=status.HTTP_200_OK)

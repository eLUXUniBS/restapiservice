#  Copyright (c) 2020. Designed, Developed, Tested and Released for University of Brescia https://www.unibs.it  All rights Reserved
import os

if os.environ.get("ENV_DEV") is not None:
    from .settings import *
else:
    from .prod_settings import *

#  Copyright (c) 2020. Designed, Developed, Tested and Released for University of Brescia https://www.unibs.it  All rights Reserved
import os
from .settings import *

ALLOWED_HOSTS = ["*"]
SECRET_KEY = os_env.get("SENSOR_API_SRV_SECRET_KEY", '8i7cf@gg)coxthlh(%ibh0626mmju32k_o+wu4ud&^qto19l#=')
DEBUG = False
DEFAULT_LOGGING_LEVEL ="DEBUG" if DEBUG else "WARNING"

# _config = dict(
#     user=os.environ.get("WORKER_USER","worker"),
#     password=os.environ.get("WORKER_PASSWORD","worker"),
#     hostname=os.environ.get("WORKER_HOSTNAME","127.0.0.1"),
#     port=os.environ.get("WORKER_PORT","5672"),
#     virtualhost=os.environ.get("WORKER_VIRTUALHOST","dev_elux_apprespiro"))
# CELERY_IGNORE_RESULT = False
# CELERY_BROKER_URL = "amqp://{user}:{password}@{hostname}:{port}/{virtualhost}".format(**_config)
# CELERY_TASK_DEFAULT_QUEUE = "queueWebService"
# CELERY_TASK_DEFAULT_EXCHANGE = "exchangeIO"
# CELERY_TASK_DEFAULT_EXCHANGE_TYPE = "topic"
# CELERY_TASK_DEFAULT_ROUTING_KEY = "elux.apprespiro.web.*"
# CELERY_CREATE_MISSING_QUEUES = False
# CELERY_TASK_QUEUE = {
#     CELERY_TASK_DEFAULT_QUEUE: {
#         "exchange": CELERY_TASK_DEFAULT_EXCHANGE,
#         "exchange_type": CELERY_TASK_DEFAULT_EXCHANGE_TYPE,
#         "binding_key": CELERY_TASK_DEFAULT_ROUTING_KEY
#     }
# }
#
# CELERY_TASK_ROUTES = {
#     "crea_profilazione.tasks.verifica_uuid": {'routing_key': 'elux.apprespiro.web.verifica_uuid'},
#     "crea_profilazione.tasks.*": {'queue': CELERY_TASK_DEFAULT_QUEUE}
# }
#
# CELERY_RESULT_BACKEND='amqp'
# CELERY_CACHE_BACKEND='amqp'
# CELERY_TASK_PROTOCOL = 1


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get("SENSOR_API_SRV_DB_NAME",'sample'),
        'USER': os.environ.get("SENSOR_API_SRV_DB_USER",'sample'),
        'PASSWORD': os.environ.get("SENSOR_API_SRV_DB_PASSWORD",'sample'),
        'HOST': os.environ.get("PGSQL_SRV_DB_HOST",'sample'),
        'PORT': os.environ.get("PGSQL_SRV_DB_PORT",'sample'),
    }
}

#!/usr/bin/env bash
echo "In caso di errori, attivare scl-enable rh-python3.X bash"
#scl enable rh-python3.6 bash
echo "Rimozione"
rm -Rf ../venv
python3 -m venv ../venv
source ../venv/bin/activate
pip3 install -r packages.pip --force-reinstall
echo "Ambiente pronto"
python3 ../service/manage.py makemigrations
python3 ../service/manage.py migrate
echo "Ambiente inizializzato"

#Configurazioni Elux
sudo chown elmos:nginx ../ -Rf
sudo chmod 775 ../ -Rf

echo "Creazione utente"
python3 ../service/manage.py loaddata user.json

echo "Creazione certificato"
# Versione con chiave cifrata
#openssl genrsa -aes256 -out ../service/api/storage/jwk/jwtRS256.key 2048
#openssl rsa -pubout -in ../service/api/storage/jwk/jwtRS256.key -out ../service/api/storage/jwk/jwtRS256.key.pub
# Versione senza chiave cifrata
ssh-keygen -t rsa -b 4096 -m PEM -f ../service/api/storage/jwk/jwtRS256.key
openssl rsa -in ../service/api/storage/jwk/jwtRS256.key -pubout -outform PEM -out ../service/api/storage/jwk/jwtRS256.key.pub


echo "Prossimi passaggi"
echo "3) Configurazione nginx"
echo "4) Avvio Servizio"
